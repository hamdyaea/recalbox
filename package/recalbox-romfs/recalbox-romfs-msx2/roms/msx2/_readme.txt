## RECALBOX - SYSTEM MSX2 ##

Put your msx roms in this directory.

BlueMSX emulator can load ".rom/.mx1/.mx2/.dsk/.cas/.m3u/.zip" files.

This system allows to use compressed roms on .zip/.7z.
But, it is only an archive. Files inside the .zip/.7z must match with extensions mentioned above.
Each .zip/.7z file must contain only one compressed rom.


## BIOS ##

- BlueMSX (EMULATOR MSX BY DEFAULT) :
You must download the BlueMSX standalone version, available to this address : http://bluemsx.msxblue.com/rel_download/blueMSXv282full.zip
Then extract the "Databases" and "Machines" folders and add them to the bios folder of your recalbox.

- FMSX :
You must have these following files, available with fmsx distribution, in the bios folder of your recalbox :
CARTS.SHA
CYRILLIC.FNT
DISK.ROM
FMPAC.ROM
FMPAC16.ROM
ITALIC.FNT
KANJI.ROM
MSX.ROM
MSX2.ROM
MSX2EXT.ROM
MSX2P.ROM
MSX2PEXT.ROM
MSXDOS2.ROM
PAINTER.ROM
RS232.ROM
